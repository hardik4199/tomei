import { ListGroup, Card, ListGroupItem } from "react-bootstrap";
import { IAllFormInputs } from "./Home";
import UplodFileImage from "../Images/Avatar.png"


interface IProps {
  priviousStep: () => void;
  resetStep: () => void;
  allFormInputs: IAllFormInputs;
}

export function CompleteFormIValues(props: IProps) {
  const { allFormInputs, priviousStep, resetStep } = props;
  const onFormConfirm = () => {
    //Submit data into backend
    alert("Data Submited Successfully.");
    resetStep()
  }
  return (
    <>
      <Card style={{ width: '18rem' }}>
        <img src={allFormInputs.employeeProfile != "" ? allFormInputs.employeeProfile : UplodFileImage} className="img-fluid" />
        <Card.Body className="text-center">
          <Card.Title><b>{allFormInputs.employeeName}</b></Card.Title>
        </Card.Body>
        <ListGroup className="list-group-flush">
          <ListGroupItem>{allFormInputs.employeeEmail}</ListGroupItem>
          <ListGroupItem>{allFormInputs.employeeSalary}</ListGroupItem>
          <ListGroupItem> {allFormInputs.employeeMobileNumber}</ListGroupItem>
          <ListGroupItem> {allFormInputs.employeeDocumentURL}</ListGroupItem>
        </ListGroup>
        <Card.Body>
          <div className="next-btn">
            <button onClick={priviousStep} type="submit">
              Privious
            </button>
            <button onClick={onFormConfirm} type="submit">
              Save
            </button>
          </div>
        </Card.Body>
      </Card>
    </>
  );
}
