import { useState } from "react"
import '../font/OpenSans-SemiBold.ttf'
import Logo from '../Images/Logo.png'
import WizardStep1 from '../Images/Wizard-Step1.png'
import WizardStep2 from '../Images/Wizard-Step2.png'
import WizardStep3 from '../Images/Wizard-Step3.png'
import WizardStep4 from '../Images/Wizard-Step4.png'
import WizardStep5 from '../Images/Wizard-Step5.png'
import { CompleteFormIValues } from "./CompleteFormIValues"
import { EmployeeAccount1 } from "./EmployeeAccount1"
import { EmployeeAccount2 } from "./EmployeeAccount2"
import { EmployeeAccount3 } from "./EmployeeAccount3"
import { EmployeeAccount4 } from "./EmployeeAccount4"
import { Row, Col } from "react-bootstrap"

export interface IAllFormInputs {
    stepNumber: number;
    employeeName: string;
    employeeEmail: string;
    employeePassword: string;
    employeeConfirmPassword: string;
    employeeMobileNumber: string;
    employeeDocumentURL: string;
    employeeProfile: string;
    employeeSalary: string;
}
const defaultValue: IAllFormInputs = {
    stepNumber: 1,
    employeeName: '',
    employeeEmail: '',
    employeePassword: '',
    employeeConfirmPassword: '',
    employeeMobileNumber: '',
    employeeDocumentURL: '',
    employeeProfile: '',
    employeeSalary: ''
}
export interface IAllFormInputErrors {
    employeeNameError: string;
    employeeEmailError: string;
    employeePasswordError: string;
    employeeConfirmPasswordError: string;
    employeeMobileNumberError: string;
    employeeDocumentURLError: string;
    employeeSalaryError: string;
}
const defaultValueErorr: IAllFormInputErrors = {
    employeeNameError: '',
    employeeEmailError: '',
    employeePasswordError: '',
    employeeConfirmPasswordError: '',
    employeeMobileNumberError: '',
    employeeDocumentURLError: '',
    employeeSalaryError: ''
}
export function Home() {
    const [formInputState, setFormInputState] = useState<IAllFormInputs>(defaultValue)
    const [stateButton, setstateButton] = useState(false);
    const [formInputError, setformInputError] = useState(defaultValueErorr);
    const handelFileChange = (profile: any) => {
        setFormInputState({
            ...formInputState,
            employeeProfile: profile.image
        });
    }
    const handleFiledsUpdate = (value: string, filedName: string) => {
        setFormInputState({
            ...formInputState,
            [filedName]: value
        });
        filedName += "Error";
        setformInputError({
            ...formInputError,
            [filedName]: ""
        });
    }
    const isValidForm = (stepNumber: number) => {
        setstateButton(true);
        let formValid = true;
        let errorJson = {
            employeeNameError: '',
            employeeEmailError: '',
            employeePasswordError: '',
            employeeConfirmPasswordError: '',
            employeeMobileNumberError: '',
            employeeDocumentURLError: '',
            employeeSalaryError: ''
        }
        if (stepNumber === 1) {
            if (formInputState.employeeName === "") {
                formValid = false;
                errorJson.employeeNameError = "Field Required"
            }
            if (formInputState.employeeEmail === "") {
                formValid = false;
                errorJson.employeeEmailError = "Field Required"
            }
            else {
                if (!new RegExp(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/g).test(formInputState.employeeEmail)) {
                    formValid = false;
                    errorJson.employeeEmailError = "Invalid Email Address"
                }
            }
            if (formInputState.employeePassword === "") {
                formValid = false;
                errorJson.employeePasswordError = "Field Required"
            }
            else {
                if (formInputState.employeePassword.length < 8) {
                    formValid = false;
                    errorJson.employeePasswordError = "Password At Least 8 Characters"
                }
            }
            if (formInputState.employeeConfirmPassword === "") {
                formValid = false;
                errorJson.employeeConfirmPasswordError = "Field Required"
            }
            if (formInputState.employeePassword !== "" && formInputState.employeeConfirmPassword !== "") {
                if (formInputState.employeePassword !== formInputState.employeeConfirmPassword) {
                    formValid = false;
                    errorJson.employeeConfirmPasswordError = "Confirm Password Not Match"
                }
            }

        }
        else if (stepNumber === 2) {
            if (formInputState.employeeMobileNumber === "") {
                formValid = false;
                errorJson.employeeMobileNumberError = "Field Required"
            }
        }
        else if (stepNumber === 3) {
            if (formInputState.employeeSalary === "") {
                formValid = false;
                errorJson.employeeSalaryError = "Field Required"
            }
        }
        else if (stepNumber === 4) {
            if (formInputState.employeeDocumentURL === "") {
                formValid = false;
                errorJson.employeeDocumentURLError = "Field Required"
            }
        }
        else {
            return formValid;
        }
        setformInputError(errorJson);
        return formValid;
    }
    const handleNextStep = () => {
        if (isValidForm(formInputState.stepNumber)) {
            setFormInputState({
                ...formInputState,
                stepNumber: formInputState.stepNumber + 1
            })
        }
    }
    const resetStep = () => {
        setFormInputState({
            ...formInputState,
            stepNumber: 1
        })
        setFormInputState(defaultValue);
    }
    const handlePreviousStep = () => {
        setFormInputState({
            ...formInputState,
            stepNumber: formInputState.stepNumber - 1
        })
    }

    const renderSteps = () => {
        if (formInputState.stepNumber === 1)
            return <EmployeeAccount1 buttonClick={stateButton} handelFileChange={handelFileChange} formError={formInputError} handleChange={handleFiledsUpdate} allFormInputs={formInputState} nextStep={handleNextStep} />
        else if (formInputState.stepNumber === 2)
            return <EmployeeAccount2 priviousStep={handlePreviousStep} formError={formInputError} handleChange={handleFiledsUpdate} allFormInputs={formInputState} nextStep={handleNextStep} />
        else if (formInputState.stepNumber === 3)
            return <EmployeeAccount3 priviousStep={handlePreviousStep} formError={formInputError} handleChange={handleFiledsUpdate} allFormInputs={formInputState} nextStep={handleNextStep} />
        else if (formInputState.stepNumber === 4)
            return <EmployeeAccount4 priviousStep={handlePreviousStep} formError={formInputError} handleChange={handleFiledsUpdate} allFormInputs={formInputState} nextStep={handleNextStep} />
        else if (formInputState.stepNumber === 5)
            return <CompleteFormIValues resetStep={resetStep} priviousStep={handlePreviousStep} allFormInputs={formInputState} />
        else
            <></>
    }
    return (
        <>
            <section className="main-section">
                <div className="logo-image text-center">
                    <img src={Logo} alt="Logo" className="img-fluid" />
                </div>
                <Row className="text-uppercase">
                    <Col className={formInputState.stepNumber === 1 ? "" : "step-hide"}>
                        <div className={formInputState.stepNumber === 1 ? "steps-image stap-text-color site-font" : "steps-image"} >
                            <div className="step-create  step-trans"><img src={WizardStep1} alt="create account step" /></div>
                            <span> STEP 1:</span>
                            <p>Create Your Account Password</p>
                        </div>
                    </Col>
                    <Col className={formInputState.stepNumber === 2 ? "" : "step-hide"}>
                        <div className={formInputState.stepNumber === 2 ? "steps-image stap-text-color" : "steps-image"} >
                            <div className="step-create step-trans"><img src={WizardStep2} alt="Logo" /></div>
                            <span> STEP 2:</span>
                            <p>Personal Information</p>
                        </div>
                    </Col>
                    <Col className={formInputState.stepNumber === 3 ? "" : "step-hide"}>
                        <div className={formInputState.stepNumber === 3 ? "steps-image stap-text-color" : "steps-image"} >
                            <div className="step-create step-trans"><img src={WizardStep3} alt="Logo" /></div>
                            <span> STEP 3:</span>
                            <p>Employment Details</p>
                        </div>

                    </Col>
                    <Col className={formInputState.stepNumber === 4 ? "" : "step-hide"}>
                        <div className={formInputState.stepNumber === 4 ? "steps-image stap-text-color" : "steps-image"} >
                            <div className="step-create step-trans"><img src={WizardStep4} alt="Logo" /></div>
                            <span> STEP 4:</span>
                            <p>Upload Document</p>
                        </div>
                    </Col>
                    <Col className={formInputState.stepNumber === 5 ? "" : "step-hide"}>
                        <div className={formInputState.stepNumber === 5 ? "steps-image stap-text-color" : "steps-image"} >
                            <div className="step-create"><img src={WizardStep5} alt="Logo" /></div>
                            <span> STEP 5:</span>
                            <p>Complete</p>
                        </div>
                    </Col>

                </Row>
            </section>
            {renderSteps()}
        </>
    )
}