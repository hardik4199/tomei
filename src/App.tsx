import { Home } from './Components/Home';
import "./App.css";

function App() {
  return (
    <div>
      <section id="main-app">
        <div className="container">
          <div className="main-app">
            <Home />
          </div>
        </div>
      </section>
    </div>
  );
}

export default App;
